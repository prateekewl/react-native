/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  ActionSheetIOS,
  CameraRoll,
  ListView,
  StyleSheet,
  Navigator,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';

import PhotoBrowser from 'react-native-photo-browser';

const EXAMPLES = [
  {
<<<<<<< HEAD
    title: 'White Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
=======
    title: 'Single photo',
    description: 'with caption, no grid button',
    enableGrid: false,
    alwaysDisplayStatusBar: true,
    media: [{
      photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
      caption: 'Grotto of the Madonna',
    }],
  }, {
    title: 'Multiple photos',
    description: 'with captions and nav arrows',
>>>>>>> 802eef51998e76a0218d73c23f8611229afc37d4
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/26/92/4d/26924d00aded585539286dc76f651f8c.jpg',
      selected: true,
    },
     {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/84/cb/bb/84cbbb18accbcc18fbb00bc2e6ab81e4.jpg',
      selected: true,
    }, {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/71/df/19/71df1933a42e6b7c3c67fe55066d9aa5.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/bd/8e/7f/bd8e7f7a7daac0731080bd55d54f3248--vsco-effects-photography-filters.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/49/8e/69/498e6931e0e9e596190bfc6d36296383--black-and-white-vsco-theme-instagram-theme-black-and-white.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/26/92/4d/26924d00aded585539286dc76f651f8c--instagram-theme-clean-feed-white-instagram.jpg',
      selected: true,
    },
  
    {
      photo: 'https://i0.wp.com/twinfinmedia.com/wp-content/uploads/2015/11/m_reinks.jpg',
      selected: true,
    },

    {
      photo: 'http://static.wixstatic.com/media/02c661_93bb0b7448224f22ae3deccff3f81734.png/v1/fill/w_626,h_1113/02c661_93bb0b7448224f22ae3deccff3f81734.png',
      selected: true,
    },
    {
      photo: 'https://lh3.googleusercontent.com/-hpTTMpUYi_4/V7UR2TcvipI/AAAAAAAAAjo/TqV8LeAQyNQ/1471483538553.jpg%20cursor:%20pointer;',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5a/32/81/5a32818d542005e7ac4d700ee795490d.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/20/df/4b/20df4b2b68bab965f5678db689179afa.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6e/b3/60/6eb3601a4d79ef381a6ccff6102aaebb.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/originals/f8/ce/92/f8ce922b396adfe4697273cd3dfadaa2.jpg',
      selected: true,
    },
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/10950434_523454307796887_2032569362_n.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c1/8c/1d/c18c1d47c829ab1aff163b823ad619d5.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/07/43/cb/0743cb2348692688da3e3fae5489402c.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/61/6e/c1/616ec17e3e08e529514c4dfffe01e898.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/1d/c3/79/1dc379ed8fd9d7d5293051d7bd5011cd.jpg',
      selected: true,
    },
    
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a1/6d/05/a16d05ae9358bc52710072126c30a782.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5b/f8/c3/5bf8c330bbadde45e4e9100e8dbfae0e.jpg',
      selected: true,
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/94/db/24/94db24e15263e5901cec59155b535a6c.jpg',
      selected: true,

    }],
   },{
    title: 'Black Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/09/57/35/0957355249843cc589b8a218c25bbcdd.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/32/19/c0/3219c0c6fbe4dd1d95ad807d751275ba.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/fa/85/fd/fa85fd3b257b63d900b7150f08393405.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/bc/41/f6/bc41f69214ee803d5316f109bf399312.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8a/4b/48/8a4b48d32a4a51609d0d88885c43b8c7.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/a7/66/84/a7668428caa08d8b8b651143c4a0be5b.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a1/21/62/a1216257f999b3e6b32458c43c5ad242.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/0b/9e/53/0b9e53ab0f1111a25d456bdc2c219efa.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/85/3c/54/853c54492307f89943bd2ad8607072ed.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/bd/96/17/bd9617cb1e96946c081c5dd2cdb8d875.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ad/69/75/ad6975cd24edf0cff4e0762c1af08fb8.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/1c/da/24/1cda243a68f7578e91fa4ac9b0487074.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b8/a5/0c/b8a50cd051bc05484169fa5f59353945.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5a/a3/09/5aa3091ec2e566bfb0008b39f88063a9.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/50/4f/77/504f77cd79b0690312b8f16ef63983c4.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e1/8e/20/e18e203c8ea45018267395aa33751efc.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b7/ee/7f/b7ee7fd7f87ca347781d449a0dd33e29.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d8/89/e3/d889e39aac8cd802b7da795f411babef.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ad/d1/7c/add17c47ba06f653fce058a5acc341fd.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6a/99/1e/6a991e12cd5681ccd95e6ad149980333.jpg',
    },




    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/88/ea/19/88ea19fdc42c09283927d7ae55156844.jpg',
    }],
  }, 

{
    title: 'Red Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c7/7d/89/c77d89e7555d9d672fb29a0d20ef5670.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d8/42/2b/d8422bc38ba071458d3206e035046365.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/42/db/eb/42dbeb47a21b6fdbd8b72e97d950886c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/44/2d/02/442d023b927f9b88254358bd76fab13d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ad/69/75/ad6975cd24edf0cff4e0762c1af08fb8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/54/6b/b1/546bb14c96fc1a5ed09524565eaa096e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ad/d1/fc/add1fc2a470e9e146494a15e15d6a46d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/4c/1f/10/4c1f10c8cfd89ed4f363710851786b24.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/16/f3/65/16f3653852555e89c89c5da9044925bd.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/14/75/73/147573b150d387c1c32c18dddff77275.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/85/5e/ce/855eced016dd94e12a042a3f013f1f2c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/06/15/70/061570349fbe3250f37e8568f3e006f4.jpg',
    }, 
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/e15/11281969_1438258449813871_1730811209_n.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/04/12/a0/0412a07b7b602ab75d3d61fa3c270775.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a2/eb/97/a2eb979e4a00482ded2e8c3f9e529670.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a8/71/f6/a871f60563cd5cf49bd96d143df31ebb.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/fb/a4/92/fba4924048faeb28d2e71a8dbd123271.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/14/75/73/147573b150d387c1c32c18dddff77275.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ae/16/bf/ae16bf760d57a40c5cfc2002b4c2d98b.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/55/09/89/5509899a7466bd5af08d38b4771fdd38.jpg',
    }, 

    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/42/db/eb/42dbeb47a21b6fdbd8b72e97d950886c.jpg',
    }],
  }, 

{
    title: 'Pink Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/1e/c1/ff/1ec1ff9d26f14d15f07dd02066a2a434.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/2e/61/bf/2e61bf84f8096dce482934d66d8093b3.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/68/e8/bd/68e8bd19da1e54f5a993b070f678ed8a.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/3a/41/db/3a41db76329b6c55246b206c5b2e2132.jpg',
    },
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/e35/19122413_474128482928224_3442195603734396928_n.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a5/da/45/a5da4576df841d8eca892bcec39f8064.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/4c/5f/56/4c5f56570b9d16166bc019dc88c5b9c3.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/04/e4/16/04e416510bbf4825ea877efe9568abde.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f3/aa/55/f3aa550fb3e871156622fbfaeb334481.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d7/ba/ea/d7baeac9b29af11286b0c315d668edce.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8b/da/c7/8bdac78d093e062e5f1c1e110851b514.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/42/0a/29/420a291ae6e95ce79492231d4e34d6e8.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ae/a7/82/aea782fcb8379cf07ef892a91ac28d07.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e4/83/59/e483592caaf78f27edffe1a918ab92df.jpg',
    },
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/cb/8b/61/cb8b61d01106ff515627b93a67f54916.jpg',
    },
    {
      photo: 'https://i0.wp.com/www.rizanoia.com/wp-content/uploads/2016/08/vsco-cam-filters-pink-instagram-feed-10.jpg?w=564',
    },
    {
      photo: 'https://i2.wp.com/www.rizanoia.com/wp-content/uploads/2016/08/vsco-cam-filters-pink-instagram-feed-13.jpg?w=500',
    },
    {
      photo: 'https://i0.wp.com/www.rizanoia.com/wp-content/uploads/2016/08/vsco-cam-filters-pink-instagram-feed-14.jpg?w=564',
    },
    {
      photo: 'https://i0.wp.com/www.rizanoia.com/wp-content/uploads/2016/08/vsco-cam-filters-pink-instagram-feed-17.jpg?w=564',
    },
    {
      photo: 'https://i1.wp.com/www.rizanoia.com/wp-content/uploads/2016/08/vsco-cam-filters-pink-instagram-feed-20.jpg?w=480',
    },

    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/07/78/64/07786434a13cf535ce303ccc5dd02dc3.jpg',
    }],
  }, 

  {
    title: 'Orange Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://cache.gmo2.sistacafe.com/images/uploads/content_image/image/61451/1448420169-10932564_785708311519588_2052018739_n.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/45/c5/ee/45c5ee84b6fbb31297a27694be53601f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f8/13/95/f81395a4b5bbc3a7bc3561f5d5b836aa.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/34/c0/26/34c02621686b3cb767c08fdb6b20d95b.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/68/9a/7c/689a7c8c4064009384f887430911409f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/51/c8/3c/51c83ca32c79c27e27d125c6901ffc13.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b9/73/35/b97335e775a5f9e01b98c3271a73e919.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e6/a4/fa/e6a4fac92fe425e59d9a503bbeb1d762.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/4c/40/a9/4c40a93b97ab13f0dc01026ae580d58a.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/3e/c9/a1/3ec9a199ab267752b1f9fec75a6e409e--photography-filters-fall-photography.jpg',
    }, 
    {
      photo: 'http://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/11820592_1552728798302305_865059543_n.jpg?ig_cache_key=MTA3OTkxMTYyOTI1ODg4NjY5MA%3D%3D.2',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/9c/f1/ba/9cf1ba9a37ded933ef9a8c8560329f98--vsco-filter-new-fonts.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/06/ec/94/06ec9493fd22f388f4314b6d68653214--vsco-filter-instagram-feed.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/d1/d9/c3/d1d9c3ffbf87219ac7dc397c1f97bc78--vsco-filter-orange.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/45/c5/ee/45c5ee84b6fbb31297a27694be53601f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b0/e3/8d/b0e38d677eaa50445af02fe233a4056a.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/36/0e/4e/360e4e81ab507b9f857750e689e71587.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a4/ee/6f/a4ee6f9ae9bc651a1e0f16586f5d0884.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e0/07/5f/e0075fb166302268bda51943a6d4c86a.jpg',
    }, 
    {
      photo: 'http://scontent-sea1-1.cdninstagram.com/t51.2885-15/s480x480/e35/12081218_1665045697045357_1659319270_n.jpg?ig_cache_key=MTA5NTc3NjEwNTA0MDMxNzYwMQ%3D%3D.2',
    }, 

    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/60/41/74/604174b6577fbf7ca4240040fb73cdc3.jpg',
    }],
  }, 

  {
    title: 'Yellow Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/cd/92/02/cd9202dcff17a3852913c86141535ca5.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/4c/51/da/4c51da7780116e3f4aad4b36781a49c5.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c6/68/c4/c668c44eb3445e072aba1c0de7e3572e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6e/60/2f/6e602fa7fe8db112ef824ef87880bd0f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/fb/5d/30/fb5d308d96a5bfdd9cd1465297d7d052.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/65/3c/cc/653ccc2661818ffe0d37495ea687ae6f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d2/74/ec/d274ecbba1568c78a49775dd86e5987a.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/64/7b/57/647b573022ea307e44b91c11f4ff2dba.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/32/9b/85/329b85cbad8183a1092a44738558e2c1.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8d/17/49/8d17493bdd7408ff4a519b7905b620d6.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a7/ae/db/a7aedbce9df56ee4ca4014a4f98390f1.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a1/da/56/a1da56c3bbac09178f5881ef5ac3077d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e6/a4/fa/e6a4fac92fe425e59d9a503bbeb1d762.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b0/24/1c/b0241cf41dc582909a49ca77efdcabb8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f0/d9/d3/f0d9d3cd42602670ac994116f4ea174e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/38/a3/49/38a349173eadc1bbad08780c2b15c72b.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/70/72/c9/7072c93ea12c216b71f4a09a628d8b0a.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ba/27/f6/ba27f683133764f8e607c7779ec3f317.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/9f/93/77/9f9377e9d028027f271a28a2c10daa58--best-vsco-filters-vsco-filters-pastel.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/originals/32/9b/85/329b85cbad8183a1092a44738558e2c1.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/originals/94/3a/4f/943a4fc1b26c2c291b2b04e4cb826d11.jpg',
    }],
  }, 


 {
    title: 'Green Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e4/82/3d/e4823d8cbdd6f700c8032eb443399dfb.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/3d/f7/04/3df7040ab084f48071ad0ef586908687.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/9d/0e/7b/9d0e7b2601c5985165150571673cb07e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c6/72/74/c67274094491e83dae63684c3676d181.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e5/fe/c9/e5fec99fd9990987c8df97167f7b14cf.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6c/4a/ea/6c4aea5b0421ec2713eb05e9052819f2.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f9/2a/03/f92a0320fd75f37545f7d4a95b59cbca.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/53/b7/b4/53b7b47ef943aa22134c85bafe0ddc1d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/3b/98/69/3b9869e05de1da92a2579f6686a31078.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f4/36/bd/f436bd95f09617322cfed7f7bd11c647.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/7b/cb/ab/7bcbab97448bd86667d14adafccbb8d9.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5c/87/f7/5c87f7472ddf89cabd37656a5528c9fa.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ed/5d/a5/ed5da5c468dcf4c7d9b2135efb224994.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/89/f3/1c/89f31c94ab8aa73aa5dea1026d1a8269.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/68/a8/f1/68a8f14ebcefcf763e2b626c028351e1.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/aa/a2/80/aaa2802f8700f93b21caaa14b33d13b7.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6d/dc/60/6ddc6011c3785be2f194f7331aa7bfec.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d9/91/c2/d991c281b29d1755f82f9b933fe37977.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/a0/75/ef/a075efa867e1de417c01e4513a64b2ff.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/14/07/2c/14072cd02c23161c45bdf6dd246c8396.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b5/d0/3c/b5d03c716afb26a5b8d33b288f0b4714.jpg',
    }],
  }, 
  {
    title: 'Blue Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/56/c6/2d/56c62d4e4b077fc37376d26bb2cc0fb8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/96/0d/61/960d61257b9b3eb4791cd238ed03e9c4.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/41/7a/93/417a9331fdba786e044d402610479171.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/58/e4/63/58e4633f14255256db25f8c44115efc1.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/41/8d/f4/418df44a06d0efa0ab3ae1d183fb18c3.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/80/b9/cc/80b9cc85db08bfd9a3076573a46e230c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/05/de/92/05de927240ba9546e318c0083e63a671.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/86/47/cb/8647cb6bda0bb0f3f81a3abb83c70d47.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8c/bb/12/8cbb1255532de4628dec92251bbceff0.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5d/fe/c0/5dfec005fbfcaab96bdbea78d9204ca6.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c7/83/d7/c783d730d66eb728e30302e57bfd8bcc.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/84/cb/bb/84cbbb18accbcc18fbb00bc2e6ab81e4.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/9b/91/b3/9b91b334f96a2dc42309c5e1b228ab2c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/1c/53/5f/1c535f478d480064ede6a2ae7bafcbcb.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/46/0a/f7/460af7f434bdb1f275f1aadc47c13a6e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/fc/fc/ce/fcfcced6eb371173fc5fd2b884b24202.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/9e/9e/91/9e9e9143c3d040490274e8f8f5573c9d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/dc/21/39/dc213909304bb8c5b0c79efac8ed4361.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ca/c2/a7/cac2a74fceddc781b29a76e3bdef22e8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/5f/b5/de/5fb5dec6d69b6492d3b7e3fd35770db4.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/aa/bd/b6/aabdb662e824c62bfb00bf6c5a0897fb.jpg',
    }],
  }, 

  // {
  //   title: 'Yellow Theme',
  //   startOnGrid: true,
  //   description: 'With filter ideas for your theme',
  //   displayNavArrows: true,
  //   displayActionButton: true,
  //   media: [{
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
  //   }, 
  //   {
  //     photo: 'http://farm3.static.flickr.com/2449/4052876281_6e068ac860_b.jpg',
  //   }],
  // }, 

  {
    title: 'Turquoise Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/6c/20/95/6c20956b98448dd177db7abfce82817d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/78/cd/ba/78cdba36b9cfa9685f89756c33b83ab7.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/69/69/f3/6969f377bee1ce645fae7a3af0d60b58.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/bf/37/44/bf3744e1636f77437490addf1e5440c2--vsco-photography-photography-guide.jpg',
    }, 
    {
      photo: 'http://data.whicdn.com/images/183690634/original.jpg',
    }, 
    {
      photo: 'http://data.whicdn.com/images/203674169/superthumb.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/3b/98/69/3b9869e05de1da92a2579f6686a31078.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/37/8a/91/378a91f905adab13e1dc634f23b9fe5f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/a7/8c/19/a78c19842487cccd0492694d99d5f213--photography-filters-vsco-photography.jpg',
    }, 
    {
      photo: 'https://lh3.googleusercontent.com/iliWOTV31ScWcpesjL0vSc5wlnPkbQsQ2M4fHwgAiVZxYo0aR0aCMeWUcr9xdlo306WNXOFB6uviGq0MQMdtIzCrhHmun-WBzwuFi2GlrSstgbupegES2WalPffwO5ZroxBI8MDd0ipEgtf4UYMjz5UCUOrDeKWdz_8mmpcDi8cptTeFSiRfPVGh-VtY5kK-kI302QSEoqNTY6k2VxMu-0asOzHr6Oa9qtFme5eIfw6H40dgL8emJzsJh4i7AcCprNRdh8hmQT4znPomXIWOV4U90ejEtCwjPpVaw3DIp4-UhGZE78HALNQFlJihf6JCU6Oy9V3PajAylNkjDRKq9ApQFfLi3rnQ_ggyfOM0YZ9TfITSsG1nJKuYXsXZiuDyU5ORDcemhQXAaboB-YmlC_Dkh65FVTYLeDgq4zIqmzgx65Sm1P1CezEBjxWyqYBmmuTGSQWM8Fxr3P-5Z2zeLTm2Pz2uOhlnk4a0SuZOhuiDIGuysde_W16jhEI0tvgfg96xpz_y7uudM5PayC6UJp2OLa0S3d2T08a2m-vBijM5J0nbdElz_Ya5-gCedFP2ZOxEevlWXwWQVp1zTjfqTH_KA4N6kLPIkvbVQyuxExm4sFYa=s638-no',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/236x/ae/9f/f3/ae9ff32cccf0fb92638cdc5a11f286a8--vsco-themes-water-pictures.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/bd/4e/c7/bd4ec79f0761cd587dbabc9c28a41b9c--vscocam-filtros-instagram-feed.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/originals/52/24/56/522456b3ac8c20bff9c69aba2b284dfd.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/09/18/5b/09185b2b5d2147cbc3aa59c5c4a063d4--vsco-cam-filters-vsco-filter.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/736x/01/68/eb/0168ebcbf17ab1f913d634676cb1a93a.jpg',
    }, 
    {
      photo: 'http://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/12502034_1133060156745857_873499554_n.jpg?ig_cache_key=MTIxMjI3NjcyNDE5Nzg4NzMyNw%3D%3D.2',
    }, 
    {
      photo: 'http://scontent-sea1-1.cdninstagram.com/t51.2885-15/s480x480/e35/12751382_188805091507026_1557392958_n.jpg?ig_cache_key=MTIwNTIwOTkxMjkyNjkzMjA1MQ%3D%3D.2',
    }, 
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/13108933_964026707050428_1839448126_n.jpg?ig_cache_key=MTI0OTQxODc3Mzc5NTU3MzA3NQ%3D%3D.2',
    }, 
    {
      photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHLrCK3vjRW5tKmvLa8CzPzPNfVIkUD8aczDYyyvvo2lbK9kqxIA',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/28/33/5c/28335c4c60efc673a568c88c09160c67.jpg',
    }, 
    {
      photo: 'https://ak1.polyvoreimg.com/cgi/img-set/cid/213934210/id/LJDhdILN5hGv2GtYzX_htA/size/y.jpg',
    }],
  }, 

 {
    title: 'Purple Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/94/37/db/9437db26ae9424b4b5f4cd3441680d9f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/50/0e/ce/500ece24433ef65e33664bdab24f5749.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8e/0b/96/8e0b960a2b5c1d5ba6db31b366dac37e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/cd/65/f6/cd65f6b731ada8226b0760b78127d471.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/74/fc/e8/74fce8e0c7a8b782f7c60698373c0d89.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/47/a8/d2/47a8d2e106b792b81102504d14061136.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/8a/e2/60/8ae260df938d81cdd6205501d1c86630.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/04/82/d3/0482d3f5767b37b541cd1048183fe72c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/44/18/cc/4418cc4859da176ad6f7e3ec924a631f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/f6/e3/fd/f6e3fd7d6548f4229797f2f9a2ed9c86.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/70/f5/ee/70f5eeba94e9f791c4dd2794e23178e3.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/12/9d/0f/129d0fb74594ffe609b225a25a72d3ed.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/47/6d/e2/476de2221d6bc9dd981b3e5ded9d181e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/68/ea/36/68ea3647fe9b1ab94b8bae1ef5d6eb58.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/37/0f/8e/370f8ed4eccc9b586721a6ad0caceca0.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ba/6a/c0/ba6ac02fc8fd6de5c989fd6024d26fe0.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/39/ef/b1/39efb168243f7e5b19a49cb72d41864e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/b9/bd/ed/b9bded789262c5c0dd6fa4635eab1ebb.jpg',
    }, 
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/e15/11372322_978037415573859_2031388440_n.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/bb/3a/4f/bb3a4f813ef63d376f1703d8813b2cd8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ae/52/24/ae522430baf33ad0c7509110b8564520.jpg',
    }],
  }, 


  {
    title: 'Brown Theme',
    startOnGrid: true,
    description: 'With filter ideas for your theme',
    displayNavArrows: true,
    displayActionButton: true,
    media: [{
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/50/2f/aa/502faadaca7d29dfa0aef3d6e2882717.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d0/a5/84/d0a58434cda6b355b71570520a3da343.jpg',
    }, 
    {
      photo: 'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/10958536_1606531726244303_182481097_n.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/88/58/e2/8858e2d2a46d30cefc637d2e0e531c2b.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ee/fa/ab/eefaabf6353b41dc2486562ec6e027f8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/04/2c/85/042c857dda69786672cf3731bb01264f.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/7a/c0/c4/7ac0c4e4d2f1417bf1e3d5c75fb439e9.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/c2/9c/01/c29c0181ac14cb75e74796f8b4517d96.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/07/d1/8e/07d18e8cc8186e81689e18e60c86800d.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/39/dd/c5/39ddc5eb85b43775be274e0db727ce01.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/2f/94/61/2f9461fed8182ebb35a6412f00f49366.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/d3/b8/c7/d3b8c7030f6b609f63e31670b6fc7b8e.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e7/c7/a6/e7c7a66d7208a199ae0248d53e3e40f8.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ae/74/71/ae747153242b844766626cc2094ef0dd.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e4/1d/a6/e41da6975e163c25a272470661829fab.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/e6/42/98/e64298b5c5498dd4ff6633c78c1600e4.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/44/8e/da/448eda5bcd41f3f636ced087b87b6e08.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/bb/6b/31/bb6b315c227774915890bcbd43fabd85.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/ba/32/de/ba32deb2da0f71322f528b1d3ab29491.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/fe/d4/0c/fed40ce1531b2277ffc0ea4f8d7ef45c.jpg',
    }, 
    {
      photo: 'https://s-media-cache-ak0.pinimg.com/564x/64/85/3d/64853d82ca6407f3d261f4e057d8bcb3.jpg',
    }],
  }, 


// {
//     title: 'Black Theme',
//     startOnGrid: true,
//     description: 'With filter ideas for your theme',
//     displayNavArrows: true,
//     displayActionButton: true,
//     media: [{
//       photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
//     }, {
//       photo: 'http://farm3.static.flickr.com/2449/4052876281_6e068ac860_b.jpg',
//     }],
//   }, 

];


// fill 'Library photos' example with local media
// CameraRoll.getPhotos({
//   first: 30,
//   assetType: 'Photos',
// }).then((data) => {
//   const media = [];
//   data.edges.forEach(d => media.push({
//     photo: d.node.image.uri,
//   }));
//   EXAMPLES[2].media = media;
// }).catch(error => alert(error));

export default class PhotoBrowserExample extends Component {

  constructor(props) {
    super(props);

    this._onSelectionChanged = this._onSelectionChanged.bind(this);
    this._onActionButton = this._onActionButton.bind(this);
    this._renderRow = this._renderRow.bind(this);
    this._renderScene = this._renderScene.bind(this);

    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });

    this.state = {
      dataSource: dataSource.cloneWithRows(EXAMPLES),
    };
  }

  _onSelectionChanged(media, index, selected) {
    alert(`${media.photo} selection status: ${selected}`);
  }

  _onActionButton(media, index) {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showShareActionSheetWithOptions({
        url: media.photo,
        message: media.caption,
      },
      () => {},
      () => {});
    } else {
      alert(`handle sharing on android for ${media.photo}, index: ${index}`);
    }
  }

  _openExample(example) {
    this.refs.nav.push(example);
  }

  _renderRow(rowData, sectionID, rowID) {
    const example = EXAMPLES[rowID];

    return (
      <TouchableOpacity onPress={() => this._openExample(example) }>
        <View style={styles.row}>
          <Text style={styles.rowTitle}>{rowData.title}</Text>
          <Text style={styles.rowDescription}>{rowData.description}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  _renderScene(route, navigator) {
    if (route.index === 0) {
      return (
        <View style={styles.container}>
          <ListView
            style={styles.list}
            dataSource={this.state.dataSource}
            renderRow={this._renderRow}
          />
        </View>
      );
    }

    const {
      media,
      initialIndex,
      displayNavArrows,
      displayActionButton,
      displaySelectionButtons,
      startOnGrid,
      enableGrid,
      alwaysDisplayStatusBar,
    } = route;

    return (
      <PhotoBrowser
        onBack={navigator.pop}
        mediaList={media}
        initialIndex={initialIndex}
        displayNavArrows={displayNavArrows}
        displaySelectionButtons={displaySelectionButtons}
        displayActionButton={displayActionButton}
        startOnGrid={startOnGrid}
        enableGrid={enableGrid}
        useCircleProgress
        onSelectionChanged={this._onSelectionChanged}
        onActionButton={this._onActionButton}
        alwaysDisplayStatusBar={alwaysDisplayStatusBar}
      />
    );
  }

  render() {
    return (
      <Navigator
        ref="nav"
        initialRoute={{ index: 0 }}
        renderScene={this._renderScene}
      />
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
    paddingTop: 54,
    paddingLeft: 16,
  },
  row: {
    flex: 1,
    padding: 8,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    borderBottomWidth: 1,
  },
  rowTitle: {
    fontSize: 14,
  },
  rowDescription: {
    fontSize: 12,
  },
});
